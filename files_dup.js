var files_dup =
[
    [ "BlinkingLEDs.py", "BlinkingLEDs_8py.html", "BlinkingLEDs_8py" ],
    [ "ClosedLoop.py", "ClosedLoop_8py.html", [
      [ "ClosedLoop", "classClosedLoop_1_1ClosedLoop.html", "classClosedLoop_1_1ClosedLoop" ]
    ] ],
    [ "Elevator_FSM.py", "Elevator__FSM_8py.html", "Elevator__FSM_8py" ],
    [ "EncoderClass.py", "EncoderClass_8py.html", [
      [ "encoder", "classEncoderClass_1_1encoder.html", "classEncoderClass_1_1encoder" ]
    ] ],
    [ "Fibonacci.py", "Fibonacci_8py.html", "Fibonacci_8py" ],
    [ "frontWeek2.py", "frontWeek2_8py.html", "frontWeek2_8py" ],
    [ "MotorDriver.py", "MotorDriver_8py.html", "MotorDriver_8py" ],
    [ "NucleoSays.py", "NucleoSays_8py.html", [
      [ "NucleoSays", "classNucleoSays_1_1NucleoSays.html", "classNucleoSays_1_1NucleoSays" ]
    ] ],
    [ "NucleoSaysTask.py", "NucleoSaysTask_8py.html", "NucleoSaysTask_8py" ],
    [ "UI_front_week1.py", "UI__front__week1_8py.html", "UI__front__week1_8py" ],
    [ "UI_nucleo_week1.py", "UI__nucleo__week1_8py.html", "UI__nucleo__week1_8py" ],
    [ "UI_nucleoMainWeek1.py", "UI__nucleoMainWeek1_8py.html", "UI__nucleoMainWeek1_8py" ]
];