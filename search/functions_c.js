var searchData=
[
  ['sawtooth_197',['sawtooth',['../BlinkingLEDs_8py.html#a2f1a2e3a001a6d8122ac11c68192d33e',1,'BlinkingLEDs']]],
  ['sendcommand_198',['sendCommand',['../frontWeek2_8py.html#afaa81db01e97afd8f154152445478d74',1,'frontWeek2.sendCommand()'],['../UI__front__week1_8py.html#a9f1c038e567c0c4cdd2d3f779b61bde6',1,'UI_front_week1.sendCommand()']]],
  ['set_5fduty_199',['set_duty',['../classMotorDriver_1_1MotorDriver.html#a4bb86eafa05d8e874896aef624ad14cd',1,'MotorDriver::MotorDriver']]],
  ['set_5fki_200',['set_Ki',['../classClosedLoop_1_1ClosedLoop.html#a1ae21155dc82f8996a4bc866f0ffc83b',1,'ClosedLoop::ClosedLoop']]],
  ['set_5fkp_201',['set_Kp',['../classClosedLoop_1_1ClosedLoop.html#ad7c23cefb90beab56623c28b0b0ca979',1,'ClosedLoop::ClosedLoop']]],
  ['set_5fposition_202',['set_position',['../classEncoderDriver_1_1encoder.html#a35b3eaae3adca11ea92e6cb91c232fa3',1,'EncoderDriver::encoder']]],
  ['sine_203',['sine',['../BlinkingLEDs_8py.html#a60ee02630a06fd308f40840d6bbe2152',1,'BlinkingLEDs']]],
  ['step_204',['step',['../BlinkingLEDs_8py.html#adeaffbad74e76620213acec192c60e3c',1,'BlinkingLEDs']]]
];
