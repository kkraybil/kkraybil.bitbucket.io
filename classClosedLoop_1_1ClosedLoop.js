var classClosedLoop_1_1ClosedLoop =
[
    [ "__init__", "classClosedLoop_1_1ClosedLoop.html#a4cea6bab4bd9d8aa4dec477adce9f984", null ],
    [ "get_Kp", "classClosedLoop_1_1ClosedLoop.html#a38ef1cd742f356fcfad3e999b91e6242", null ],
    [ "run", "classClosedLoop_1_1ClosedLoop.html#adf1811b64f0e02ac48ced1c6ff15279b", null ],
    [ "set_Ki", "classClosedLoop_1_1ClosedLoop.html#a1ae21155dc82f8996a4bc866f0ffc83b", null ],
    [ "set_Kp", "classClosedLoop_1_1ClosedLoop.html#ad7c23cefb90beab56623c28b0b0ca979", null ],
    [ "Ki", "classClosedLoop_1_1ClosedLoop.html#af5c07eb738bea6f8f038dcc7a46024e7", null ],
    [ "Kp", "classClosedLoop_1_1ClosedLoop.html#a600c2ee167190f2afdf9fc38c849cf01", null ],
    [ "L", "classClosedLoop_1_1ClosedLoop.html#add9a36f67edcf30547d56b1676d53b92", null ],
    [ "omega", "classClosedLoop_1_1ClosedLoop.html#a9304e47dbe7c13bf3c847cd79ce3b59f", null ],
    [ "omega_des", "classClosedLoop_1_1ClosedLoop.html#ae53525bde23bd271f7c2f875fe1d8897", null ],
    [ "position", "classClosedLoop_1_1ClosedLoop.html#aa65ca22adde64f5906c54b24b6354ba9", null ],
    [ "position_des", "classClosedLoop_1_1ClosedLoop.html#a90666ccd5bf7c77b11938f179a9f0527", null ]
];