var classnucleoUIWeek3_1_1dataCollect =
[
    [ "__init__", "classnucleoUIWeek3_1_1dataCollect.html#a7a7adf3199f44223bb43256601e445a8", null ],
    [ "run", "classnucleoUIWeek3_1_1dataCollect.html#a5bfece552519a271d14ce4a2350048ab", null ],
    [ "transitionTo", "classnucleoUIWeek3_1_1dataCollect.html#a338e83186ab52a09e3095699a15247c5", null ],
    [ "counter", "classnucleoUIWeek3_1_1dataCollect.html#ac239e3d8b589bb3a71147fdd8853cd88", null ],
    [ "DBG_flag", "classnucleoUIWeek3_1_1dataCollect.html#a96a569706f5090e3efb324808a36db99", null ],
    [ "Kp", "classnucleoUIWeek3_1_1dataCollect.html#a1dd872c8a9d37c1da4e6dafd28edad6f", null ],
    [ "lastTime", "classnucleoUIWeek3_1_1dataCollect.html#ac1c5276157c42148a1001dccdd910457", null ],
    [ "nextTime", "classnucleoUIWeek3_1_1dataCollect.html#a247cbbb0a90aad67cf3f2dd1018460b2", null ],
    [ "period", "classnucleoUIWeek3_1_1dataCollect.html#adc900f180cf5c987acdc93840185a7e0", null ],
    [ "runs", "classnucleoUIWeek3_1_1dataCollect.html#ac81f1665732fc85489a6bd4b02ac4feb", null ],
    [ "state", "classnucleoUIWeek3_1_1dataCollect.html#af58969e918d4d048adb43284c3ef984f", null ],
    [ "time", "classnucleoUIWeek3_1_1dataCollect.html#aae645adc5730e1a03695a8ad5a1df780", null ],
    [ "times", "classnucleoUIWeek3_1_1dataCollect.html#a7a94688bca99381cd89bb877f1967a0f", null ],
    [ "val", "classnucleoUIWeek3_1_1dataCollect.html#a0e5999c2c1b106bf4ecc3be42efa8cbd", null ],
    [ "values", "classnucleoUIWeek3_1_1dataCollect.html#ad57fdcb3b17b4eddb0e1a86b702c7147", null ]
];