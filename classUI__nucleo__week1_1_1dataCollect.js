var classUI__nucleo__week1_1_1dataCollect =
[
    [ "__init__", "classUI__nucleo__week1_1_1dataCollect.html#aa017206121dee8e5fa3d1815529fc5b3", null ],
    [ "run", "classUI__nucleo__week1_1_1dataCollect.html#aba6d0d62ffb2bfe6417dd72e7a2d57cd", null ],
    [ "transitionTo", "classUI__nucleo__week1_1_1dataCollect.html#aceae4125fde98e6cbbb2f20d4e7d708b", null ],
    [ "DBG_flag", "classUI__nucleo__week1_1_1dataCollect.html#a20972f71c91db72274cf6120a6f5422d", null ],
    [ "lastTime", "classUI__nucleo__week1_1_1dataCollect.html#a4d62cc151e121527f39ec9f85805e408", null ],
    [ "nextTime", "classUI__nucleo__week1_1_1dataCollect.html#ace683a1b46db931998a9521da60f605d", null ],
    [ "period", "classUI__nucleo__week1_1_1dataCollect.html#a113d560202fedf2c8726aa4e5a61e604", null ],
    [ "runs", "classUI__nucleo__week1_1_1dataCollect.html#a1f9bb820ef0549e686078152349e1719", null ],
    [ "state", "classUI__nucleo__week1_1_1dataCollect.html#ac05cf247273915e72d1428e48161dd56", null ],
    [ "time", "classUI__nucleo__week1_1_1dataCollect.html#a2a91bdfb95531c8d911655edb10e5611", null ],
    [ "times", "classUI__nucleo__week1_1_1dataCollect.html#a4aae7e06d39cf7f550314d64d1fd1699", null ],
    [ "val", "classUI__nucleo__week1_1_1dataCollect.html#a4c70103c536d42625b01bc2e5bddac4d", null ],
    [ "values", "classUI__nucleo__week1_1_1dataCollect.html#a69d90913af3a289a48a8011f6644728d", null ]
];