var frontWeek2_8py =
[
    [ "kb_cb", "frontWeek2_8py.html#a3d7a2d79073af4ea75d2bedd9846be65", null ],
    [ "sendCommand", "frontWeek2_8py.html#afaa81db01e97afd8f154152445478d74", null ],
    [ "color", "frontWeek2_8py.html#a1a6a27fe356b9cea3d51d33bdb6694a6", null ],
    [ "counter", "frontWeek2_8py.html#a86aad068ac76ae817972cda58e0171ad", null ],
    [ "data", "frontWeek2_8py.html#aa97b16e614de49053d4737deb1145a3d", null ],
    [ "firstRun", "frontWeek2_8py.html#ab77e3733476d1b722f5da493f2fd86f6", null ],
    [ "last_key", "frontWeek2_8py.html#a3a90cfc06ec1b91f5e2929e1afed8f00", null ],
    [ "mySplitString", "frontWeek2_8py.html#afab8a89037920d745aee75e66f1de667", null ],
    [ "myStrippedString", "frontWeek2_8py.html#a2394ed4f441e961b5c016bc8da6000bb", null ],
    [ "plotter", "frontWeek2_8py.html#aa65aaf18b462d174be61e3848a0dca5c", null ],
    [ "ser", "frontWeek2_8py.html#a79aa9f19e9119afed0b3e320fda681d2", null ],
    [ "times", "frontWeek2_8py.html#ac05bdc6d93929af5295592a8fb313302", null ],
    [ "values", "frontWeek2_8py.html#adc27800d5cedac840d52a8758f3aefab", null ],
    [ "writer", "frontWeek2_8py.html#a044797b97a7e914013b9ca4a952ffbed", null ],
    [ "x_times", "frontWeek2_8py.html#ac2d404498252fac06452fb7d091a515c", null ],
    [ "y_data", "frontWeek2_8py.html#ad738a96c0c33d48054277f6cf6d45e83", null ]
];