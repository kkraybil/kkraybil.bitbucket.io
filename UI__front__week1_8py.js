var UI__front__week1_8py =
[
    [ "kb_cb", "UI__front__week1_8py.html#a2684a3ce07a0cb7cfa0b92b2600c7b1f", null ],
    [ "sendCommand", "UI__front__week1_8py.html#a9f1c038e567c0c4cdd2d3f779b61bde6", null ],
    [ "color", "UI__front__week1_8py.html#aeb4c978bef74b541e7b7aa6d48bef8fb", null ],
    [ "counter", "UI__front__week1_8py.html#ab8b91d30c8263aef61227efeeb5716d5", null ],
    [ "data", "UI__front__week1_8py.html#a58143929756d18649e2ceb919e7451f5", null ],
    [ "firstRun", "UI__front__week1_8py.html#aa5ecd5bb945eb42433e16d2905fdeab8", null ],
    [ "last_key", "UI__front__week1_8py.html#a179873bf0afab90300386d1c333d5a1d", null ],
    [ "mySplitString", "UI__front__week1_8py.html#ab768c40461cea842663fedbd70b66df4", null ],
    [ "myStrippedString", "UI__front__week1_8py.html#acc032e947bf31e5dcc9938db9eda67c0", null ],
    [ "plotter", "UI__front__week1_8py.html#a9457c8ef38a312543d6b76f8e66a3672", null ],
    [ "ser", "UI__front__week1_8py.html#a52d34d1dad880f3097dbf76253705c33", null ],
    [ "times", "UI__front__week1_8py.html#aadae499463f6505463c632fd20613086", null ],
    [ "values", "UI__front__week1_8py.html#a6a250ee9094fe85fffb4f23e2f3fcff6", null ],
    [ "writer", "UI__front__week1_8py.html#a9d8c6ee3081c51b4a539f168481aa6f5", null ],
    [ "x_times", "UI__front__week1_8py.html#a0a9c25f8bb017e50942f7a11578731d3", null ],
    [ "y_data", "UI__front__week1_8py.html#a1208369328beb73a401c603ab5a4d281", null ]
];