var classnucleoUIWeek2_1_1dataCollect =
[
    [ "__init__", "classnucleoUIWeek2_1_1dataCollect.html#a6174e59a8a688412358d2860cd37a15a", null ],
    [ "run", "classnucleoUIWeek2_1_1dataCollect.html#ac29758cb9a89aeced3f689bbf2ec9671", null ],
    [ "transitionTo", "classnucleoUIWeek2_1_1dataCollect.html#adeecab6038de0ab8005f92b8f0528fc2", null ],
    [ "DBG_flag", "classnucleoUIWeek2_1_1dataCollect.html#ad909d27a180c7cf012407cde081190b3", null ],
    [ "encoder", "classnucleoUIWeek2_1_1dataCollect.html#ad872ddcbf868693280e8f60d7c92f946", null ],
    [ "lastTime", "classnucleoUIWeek2_1_1dataCollect.html#a8eaaa38bc5a18a84d4ed791c93739663", null ],
    [ "nextTime", "classnucleoUIWeek2_1_1dataCollect.html#ae58c3a5c3fa092d3593c1fd65ee03b6d", null ],
    [ "period", "classnucleoUIWeek2_1_1dataCollect.html#a827712bdb16cee588e90c3f805824281", null ],
    [ "runs", "classnucleoUIWeek2_1_1dataCollect.html#ab9071839d8877642e628c644d2488d99", null ],
    [ "state", "classnucleoUIWeek2_1_1dataCollect.html#a0f6bb99586575c4151b3689f94bfee93", null ],
    [ "time", "classnucleoUIWeek2_1_1dataCollect.html#a66a2fec48aae63ea608b8a7dde8d4140", null ],
    [ "times", "classnucleoUIWeek2_1_1dataCollect.html#ad51b52f4f7f7b5dca6496e3218cdb950", null ],
    [ "val", "classnucleoUIWeek2_1_1dataCollect.html#a9bf91f92359f40f6b20f6c67f76d8384", null ],
    [ "values", "classnucleoUIWeek2_1_1dataCollect.html#ace766d68c5cd9e0c635d69c7645819fe", null ]
];