var annotated_dup =
[
    [ "ClosedLoop", null, [
      [ "ClosedLoop", "classClosedLoop_1_1ClosedLoop.html", "classClosedLoop_1_1ClosedLoop" ]
    ] ],
    [ "ControlTaskWeek3", null, [
      [ "controlTask", "classControlTaskWeek3_1_1controlTask.html", "classControlTaskWeek3_1_1controlTask" ]
    ] ],
    [ "ControlTaskWeek4", null, [
      [ "controlTask", "classControlTaskWeek4_1_1controlTask.html", "classControlTaskWeek4_1_1controlTask" ]
    ] ],
    [ "EncoderClass", null, [
      [ "encoder", "classEncoderClass_1_1encoder.html", "classEncoderClass_1_1encoder" ]
    ] ],
    [ "EncoderDriver", null, [
      [ "encoder", "classEncoderDriver_1_1encoder.html", "classEncoderDriver_1_1encoder" ]
    ] ],
    [ "MotorDriver", null, [
      [ "MotorDriver", "classMotorDriver_1_1MotorDriver.html", "classMotorDriver_1_1MotorDriver" ]
    ] ],
    [ "NucleoSays", null, [
      [ "NucleoSays", "classNucleoSays_1_1NucleoSays.html", "classNucleoSays_1_1NucleoSays" ]
    ] ],
    [ "nucleoUIWeek2", null, [
      [ "dataCollect", "classnucleoUIWeek2_1_1dataCollect.html", "classnucleoUIWeek2_1_1dataCollect" ]
    ] ],
    [ "nucleoUIWeek3", null, [
      [ "dataCollect", "classnucleoUIWeek3_1_1dataCollect.html", "classnucleoUIWeek3_1_1dataCollect" ]
    ] ],
    [ "UI_nucleo_week1", null, [
      [ "dataCollect", "classUI__nucleo__week1_1_1dataCollect.html", "classUI__nucleo__week1_1_1dataCollect" ]
    ] ]
];